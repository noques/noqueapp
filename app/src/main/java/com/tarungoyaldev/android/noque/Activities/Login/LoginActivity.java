package com.tarungoyaldev.android.noque.Activities.Login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.tarungoyaldev.android.noque.Activities.StoreFinder.StoreFinderActivity;
import com.tarungoyaldev.android.noque.Activities.StoreService;
import com.tarungoyaldev.android.noque.App;
import com.tarungoyaldev.android.noque.DaggerActivity;
import com.tarungoyaldev.android.noque.R;

import java.util.HashMap;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends DaggerActivity {

    private static final String TAG = "LoginActivity";
    @Inject
    StoreService storeService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);
        Log.e("LoginActivity", "!!!!!!!Login View!!!!!!");
    }

    @Override
    protected Object getModule() {
        return new LoginModule(this);
    }

    public void login(View view) {
        storeService.loginCustomer(getLoginBody())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(customer -> {
                    ((App) getApplication()).setCustomer(customer);
                    Intent intent = new Intent(LoginActivity.this, StoreFinderActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }, error -> {
                    Log.e(TAG, error.toString());
                });
    }

    private HashMap<String, String> getLoginBody() {
        HashMap<String, String> loginHash = new HashMap<>();
        String username = ((EditText) findViewById(R.id.usernameEditText)).getText().toString();
        String password = ((EditText) findViewById(R.id.passwordEditText)).getText().toString();
        loginHash.put("username", username);
        loginHash.put("password", password);
        return loginHash;
    }
}
