package com.tarungoyaldev.android.noque.Activities.SplashScreen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.tarungoyaldev.android.noque.Activities.Login.LoginActivity;
import com.tarungoyaldev.android.noque.Activities.StoreFinder.StoreFinderActivity;
import com.tarungoyaldev.android.noque.App;
import com.tarungoyaldev.android.noque.Config;
import com.tarungoyaldev.android.noque.DaggerActivity;
import com.tarungoyaldev.android.noque.R;

import javax.inject.Inject;

public class SplashScreenActivity extends DaggerActivity {

    @Inject
    Config config;

    private static final String TAG = "SplashScreenActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_splash_screen);
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String authToken = sharedPref.getString(getResources().getString(R.string.auth_token), null);
        if (authToken == null) {
            Log.i(TAG, "Authtoken not found!!!");
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            Log.e(TAG, "Authtoken found!!!");
            ((App) getApplication()).initiateCustomer();
            Intent intent = new Intent(this, StoreFinderActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected Object getModule() {
        return new SplashScreenModule(this);
    }
}
