package com.tarungoyaldev.android.noque.Activities.StoreDetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.annotations.SerializedName;
import com.tarungoyaldev.android.noque.Activities.StoreFinder.StoreFinderActivity;
import com.tarungoyaldev.android.noque.Activities.StoreService;
import com.tarungoyaldev.android.noque.Activities.barcode.BarcodeCaptureActivity;
import com.tarungoyaldev.android.noque.Config;
import com.tarungoyaldev.android.noque.DrawerNavigationActivity;
import com.tarungoyaldev.android.noque.R;
import com.tarungoyaldev.android.noque.model.StoreInformation;
import com.tarungoyaldev.android.noque.model.StoreProduct;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class StoreDetailsActivity extends DrawerNavigationActivity {


    private Observable<StoreService> storeApiObservable;

    private ListView storeListView;
    private StoreDetailsFragment storeDetailsFragment;

    private final String TAG = "StoreDetailsActivity";
    private static final int RC_BARCODE_CAPTURE = 9001;

    public static final String STORE_ARG = "Store arg";
    public static final String QRCODE = "QRCode";
    public static final String STORE_PRODUCT = "Store Product";

    @Inject Config config;
    @Inject StoreInformation storeInformation;
    @Inject StoreService storeService;
    @Inject ArrayList<StoreProduct> storeProductList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_store_details);
        super.onCreate(savedInstanceState);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.bringToFront();
            fab.setClickable(true);
            System.out.println("Floating button is not null!!!");
            System.out.println("Floating button clickable: " + fab.isClickable());
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("Fab button clicked!!!!!!");
                    Intent intent = new Intent(StoreDetailsActivity.this, BarcodeCaptureActivity.class);
                    intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
                    intent.putExtra(BarcodeCaptureActivity.UseFlash, false);

                    startActivityForResult(intent, RC_BARCODE_CAPTURE);
                }
            });
        }

        if (findViewById(R.id.storeMainLayout) != null) {

            if (savedInstanceState != null) {
                return;
            }

            storeDetailsFragment = new StoreDetailsFragment();

            storeDetailsFragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.storeMainLayout, storeDetailsFragment).commit();
        }

        storeInformation.setStoreInformation(
                (StoreInformation) getIntent().getSerializableExtra(StoreFinderActivity.STOREINTENTEXTRA));

        TextView storeNameTextView = (TextView) findViewById(R.id.storeName_storeDetails);
        TextView storeLocationTextView = (TextView) findViewById(R.id.storeLocation_storeDetails);
        storeNameTextView.setText(storeInformation.getStoreName());
        storeLocationTextView.setText(storeInformation.getLocation());
    }

    public void checkout(View view) {
        storeService.placeOrder(getOrderBody())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(placeOrderResponse -> {
                    String message = placeOrderResponse.getMessage();
                    Log.i(TAG, message);
                    String orderId = String.valueOf(placeOrderResponse.getOrderId());

                    StoreRecieptQrcodeFragment storeRecieptQrcodeFragment = new StoreRecieptQrcodeFragment();
                    Bundle args = new Bundle();
                    args.putString(QRCODE, orderId);
                    storeRecieptQrcodeFragment.setArguments(args);

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    transaction.replace(R.id.storeMainLayout, storeRecieptQrcodeFragment);
                    transaction.addToBackStack(null);
                    Log.e(TAG, "Added new fragment!!!");

                    transaction.commit();
                }, error -> {
                    Log.e(TAG, error.toString());
                });
    }

    private OrderBody getOrderBody() {
        ArrayList<StoreProductBody> storeProductBodyArrayList = new ArrayList<>();
        for (StoreProduct storeProduct : storeProductList) {
            StoreProductBody storeProductBody =
                    new StoreProductBody(String.valueOf(storeProduct.getId()), "2");
            storeProductBodyArrayList.add(storeProductBody);
        }
        return new OrderBody(
                String.valueOf(config.getCustomer().getUserId()),
                String.valueOf(storeInformation.getId()),
                storeProductBodyArrayList);
    }

    public class OrderBody {
        @SerializedName("user_id")
        String userId;

        @SerializedName("store_id")
        String storeId;

        @SerializedName("store_products")
        List<StoreProductBody> storeProductBodyList;

        OrderBody(String userId, String storeId, List<StoreProductBody> storeProductBodyList) {
            this.userId = userId;
            this.storeId = storeId;
            this.storeProductBodyList = storeProductBodyList;
        }
    }

    public class StoreProductBody {
        @SerializedName("store_product_id")
        String storeProductId;

        @SerializedName("quantity")
        String quantity;

        StoreProductBody(String storeProductId, String quantity) {
            this.storeProductId = storeProductId;
            this.quantity = quantity;
        }
    }

    /**
     * Called when an activity you launched exits, giving you the requestCode
     * you started it with, the resultCode it returned, and any additional
     * data from it.  The <var>resultCode</var> will be
     * {@link #RESULT_CANCELED} if the activity explicitly returned that,
     * didn't return any result, or crashed during its operation.
     * <p/>
     * <p>You will receive this call immediately before onResume() when your
     * activity is re-starting.
     * <p/>
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     *                    (various data can be attached to Intent "extras").
     * @see #startActivityForResult
     * @see #createPendingResult
     * @see #setResult(int)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "In onActivityResult");
        if (requestCode == RC_BARCODE_CAPTURE) {
            Log.e(TAG, "In onActivityResult1");
            if (resultCode == CommonStatusCodes.SUCCESS) {
                Log.e(TAG, "In onActivityResult2");
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    String barcodeString = barcode.displayValue;
                    storeDetailsFragment.addStoreProduct(barcodeString.substring(0,barcodeString.length() - 1));
                } else {
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected Object getModule() {
        return new StoreDetailsModule(this);
    }
}
