package com.tarungoyaldev.android.noque.Activities.StoreDetails;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.tarungoyaldev.android.noque.Config;
import com.tarungoyaldev.android.noque.R;
import com.tarungoyaldev.android.noque.Activities.StoreService;
import com.tarungoyaldev.android.noque.model.StoreInformation;
import com.tarungoyaldev.android.noque.model.StoreProduct;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tarungoyal on 12/07/16.
 */
public class StoreDetailsFragment extends ListFragment {

    public interface StoreProductUpdateListener {
        void addStoreProduct(StoreProduct storeProduct);
    }

    private static final String TAG = "StoreFinderFragment";

    @Inject StoreInformation storeInformation;
    @Inject Config config;
    @Inject StoreService storeService;
    @Inject ArrayList<StoreProduct> storeProductList;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((StoreDetailsActivity) getActivity()).inject(this);
        StoreProductListAdapter adapter = new StoreProductListAdapter(getContext(), storeProductList);
        setListAdapter(adapter);
    }

    void addStoreProduct(String barcode) {
        for (StoreProduct storeProduct : storeProductList) {
            if (String.valueOf(storeProduct.getProduct().getBarcodeNumber()).equals(barcode)) {
                View listItem = getListView().getChildAt(storeProductList.indexOf(storeProduct));
                EditText quantityEditText = (EditText) listItem.findViewById(R.id.quantityEditText);
                int quantity = Integer.valueOf(quantityEditText.getText().toString());
                quantity += 1;
                quantityEditText.setText(String.valueOf(quantity));
                return;
            }
        }
        storeService.getStoreProduct(storeInformation.getId(), barcode, "json")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeProduct -> {
                    storeProductList.add(storeProduct);
                    ((StoreProductListAdapter) getListAdapter()).notifyDataSetChanged();
                }, error -> {
                    Log.e(TAG, error.toString());
                });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
//        ((StoreSelectedListener) getActivity()).setSelectedStore(storeInformation.get(position));
//        Observable<StoreProduct> storeProductObservable = storeService.getStoreProduct(
//                String.valueOf(storeInformation.get(position).getId()), "1000001","json");
//
//        storeProductObservable.subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(storeProduct -> {
//                    Log.e(TAG, String.valueOf(storeProduct.getProduct()));
//                }, error -> {
//                    Log.e(TAG, error.toString());
//                });
    }
}
