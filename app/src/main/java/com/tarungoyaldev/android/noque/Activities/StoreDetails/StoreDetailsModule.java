package com.tarungoyaldev.android.noque.Activities.StoreDetails;

import com.tarungoyaldev.android.noque.AppModule;
import com.tarungoyaldev.android.noque.model.StoreInformation;
import com.tarungoyaldev.android.noque.model.StoreProduct;

import java.util.ArrayList;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tarungoyal on 12/07/16.
 */
@Module(
        injects = {
                StoreDetailsActivity.class,
                StoreDetailsFragment.class,
                StoreRecieptQrcodeFragment.class
        },
        addsTo = AppModule.class
)
public class StoreDetailsModule {
    private StoreDetailsActivity storeDetailsActivity;

    public StoreDetailsModule(StoreDetailsActivity storeDetailsActivity) {
        this.storeDetailsActivity = storeDetailsActivity;
    }

    @Provides
    @Singleton
    public StoreDetailsActivity provideStoreDetailsActivity() {
        return storeDetailsActivity;
    }

    @Provides
    @Singleton
    public StoreInformation provideStoreInformation() {
        return new StoreInformation();
    }

    @Provides
    @Singleton
    public ArrayList<StoreProduct> provideStoreProductList() {
        return new ArrayList<StoreProduct>();
    }
}
