package com.tarungoyaldev.android.noque.Activities.StoreDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.tarungoyaldev.android.noque.R;
import com.tarungoyaldev.android.noque.model.StoreProduct;

import java.util.ArrayList;

/**
 * Created by tarungoyal on 12/07/16.
 */
public class StoreProductListAdapter extends ArrayAdapter<StoreProduct> {
    private final Context context;
    private final ArrayList<StoreProduct> storeProducts;

    public StoreProductListAdapter(Context context, ArrayList<StoreProduct> storeProducts) {
        super(context, -1, storeProducts);
        this.context = context;
        this.storeProducts = storeProducts;
        setNotifyOnChange(true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        StoreProduct storeProduct = storeProducts.get(position);

        View rowView = inflater.inflate(R.layout.store_product_list_item, parent, false);
        TextView productNameTextView = (TextView) rowView.findViewById(R.id.productName);
        TextView productDescriptionTextView = (TextView) rowView.findViewById(R.id.productDescription);
        TextView productSellingPriceTextView = (TextView) rowView.findViewById(R.id.productSellingPrice);
        productNameTextView.setText(storeProduct.getProduct().getProductName());
        productDescriptionTextView.setText(storeProduct.getProduct().getShortDescription());
        productSellingPriceTextView.setText(String.valueOf(storeProduct.getStorePrice()));

        EditText quantityEditText = (EditText) rowView.findViewById(R.id.quantityEditText);
        quantityEditText.setText("1");

        return rowView;
    }


}
