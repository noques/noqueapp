package com.tarungoyaldev.android.noque.Activities.StoreDetails;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.tarungoyaldev.android.noque.R;
import com.tarungoyaldev.android.noque.model.StoreProduct;

/**
 * Created by tarungoyal on 17/07/16.
 */
public class StoreRecieptQrcodeFragment extends Fragment{

    ImageView qrCodeImageview;
    String QRcode;
    public final static int WIDTH=500;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.store_reciept_qrcode_fragment, container, false);
        qrCodeImageview = (ImageView) view.findViewById(R.id.reciept_qrcode_imageView);

        Thread t = new Thread(new Runnable() {
            public void run() {
// this is the msg which will be encode in QRcode
                QRcode=getArguments().getString(StoreDetailsActivity.QRCODE);

                synchronized (this) {
// runOnUiThread method used to do UI task in main thread.
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Bitmap bitmap = null;

                                bitmap = encodeAsBitmap(QRcode);
                                qrCodeImageview.setImageBitmap(bitmap);

                            } catch (WriterException e) {
                                e.printStackTrace();
                            } // end of catch block

                        } // end of run method
                    });

                }
            }
        });
        t.start();
        return view;
    }

    // this is method call from on create and return bitmap image of QRCode.
    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, WIDTH, WIDTH, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? getResources().getColor(R.color.black):getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 500, 0, 0, w, h);
        return bitmap;
    } /// end of this method




    private StoreProduct populateStoreProduct(String barcode) {
//        storeService.getStoreProduct(storeInformation.getId(), barcode, "json")
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(storeProduct -> {
//                    storeProductList.add(storeProduct);
//                    ((StoreProductListAdapter) getListAdapter()).notifyDataSetChanged();
//                }, error -> {
//                    Log.e(TAG, error.toString());
//                });
        return new StoreProduct();
    }
}
