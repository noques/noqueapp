package com.tarungoyaldev.android.noque.Activities.StoreFinder;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.tarungoyaldev.android.noque.Config;
import com.tarungoyaldev.android.noque.DrawerNavigationActivity;
import com.tarungoyaldev.android.noque.R;
import com.tarungoyaldev.android.noque.Activities.StoreDetails.StoreDetailsActivity;
import com.tarungoyaldev.android.noque.Activities.StoreService;
import com.tarungoyaldev.android.noque.model.StoreInformation;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Observable;

public class StoreFinderActivity extends DrawerNavigationActivity implements StoreFinderFragment.StoreSelectedListener {

    private Observable<StoreService> storeApiObservable;

    private ListView storeListView;

    private static final String TAG = "StoreFinderActivity";
    public static final String STOREINTENTEXTRA = "Store";

    @Inject Config config;
    @Inject ArrayList<StoreInformation> storeInformationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_store_finder);
        super.onCreate(savedInstanceState);

        if (findViewById(R.id.storeMainLayout) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity StoreDetails
            StoreFinderFragment storeFinderFragment = new StoreFinderFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            storeFinderFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.storeMainLayout, storeFinderFragment).commit();
        }
    }

    @Override
    public void setSelectedStore(StoreInformation store) {
        Intent intent = new Intent(this, StoreDetailsActivity.class);
        intent.putExtra(STOREINTENTEXTRA, store);
        startActivity(intent);
    }

    @Override
    protected Object getModule() {
        return new StoreFinderModule(this);
    }
}
