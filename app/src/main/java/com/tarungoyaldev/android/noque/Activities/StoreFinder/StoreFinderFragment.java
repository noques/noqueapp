package com.tarungoyaldev.android.noque.Activities.StoreFinder;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.tarungoyaldev.android.noque.Activities.StoreService;
import com.tarungoyaldev.android.noque.model.StoreInformation;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tarungoyal on 12/07/16.
 */
public class StoreFinderFragment extends ListFragment {

    public interface StoreSelectedListener {
        void setSelectedStore(StoreInformation store);
    }

    private static final String TAG = "StoreFinderFragment";
    @Inject ArrayList<StoreInformation> storeInformationList;
    @Inject
    StoreService storeService;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((StoreFinderActivity) getActivity()).inject(this);
        StoreListAdapter storeListAdapter = new StoreListAdapter(getContext(), storeInformationList);
        setListAdapter(storeListAdapter);

        Observable<List<StoreInformation>> nearbyStores = storeService.getStoreList("json");

        nearbyStores.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeInformations -> {
                    populateStoreList(storeInformations);
                }, error -> {
                    Log.e(TAG, error.toString());
                });
    }

    private void populateStoreList(List<StoreInformation> storeInformationList) {
        this.storeInformationList.clear();
        this.storeInformationList.addAll(storeInformationList);
        StoreListAdapter storeListAdapter = (StoreListAdapter) getListAdapter();
        storeListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        ((StoreFinderActivity) getActivity()).setSelectedStore(storeInformationList.get(position));
    }
}
