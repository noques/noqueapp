package com.tarungoyaldev.android.noque.Activities.StoreFinder;

import com.tarungoyaldev.android.noque.AppModule;
import com.tarungoyaldev.android.noque.model.StoreInformation;

import java.util.ArrayList;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tarungoyal on 12/07/16.
 */
@Module(
        injects = {
                StoreFinderActivity.class,
                StoreFinderFragment.class,
        },
        addsTo = AppModule.class
)
public class StoreFinderModule {
    private StoreFinderActivity storeFinderActivity;

    public StoreFinderModule(StoreFinderActivity storeFinderActivity) {
        this.storeFinderActivity = storeFinderActivity;
    }

    @Provides
    @Singleton
    public StoreFinderActivity provideStoreFinderActivity() {
        return storeFinderActivity;
    }

    @Provides
    @Singleton
    public ArrayList<StoreInformation> provideStoreInformationList() {
        return new ArrayList<StoreInformation>();
    }
}
