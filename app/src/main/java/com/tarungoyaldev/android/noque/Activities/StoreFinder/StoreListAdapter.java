package com.tarungoyaldev.android.noque.Activities.StoreFinder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tarungoyaldev.android.noque.R;
import com.tarungoyaldev.android.noque.model.StoreInformation;

import java.util.ArrayList;

/**
 * Created by tarungoyal on 12/07/16.
 */
public class StoreListAdapter extends ArrayAdapter<StoreInformation> {
    private final Context context;
    private final ArrayList<StoreInformation> storeInformations;

    public StoreListAdapter(Context context, ArrayList<StoreInformation> storeInformations) {
        super(context, -1, storeInformations);
        this.context = context;
        this.storeInformations = storeInformations;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        StoreInformation storeInformation = storeInformations.get(position);

        View rowView = inflater.inflate(R.layout.store_list_item, parent, false);
        TextView storeNameTextView = (TextView) rowView.findViewById(R.id.storeName);
        TextView storeLocationTextView = (TextView) rowView.findViewById(R.id.storeLocation);
        TextView storePhoneNumberTextView = (TextView) rowView.findViewById(R.id.storePhoneNumber);
        storeNameTextView.setText(storeInformation.getStoreName());
        storeLocationTextView.setText(storeInformation.getLocation());
        storePhoneNumberTextView.setText(String.valueOf(storeInformation.getPhone()));

        return rowView;
    }
}
