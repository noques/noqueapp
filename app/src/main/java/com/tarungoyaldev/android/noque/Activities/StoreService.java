package com.tarungoyaldev.android.noque.Activities;

import com.tarungoyaldev.android.noque.Activities.StoreDetails.StoreDetailsActivity;
import com.tarungoyaldev.android.noque.model.Customer;
import com.tarungoyaldev.android.noque.model.PlaceOrderResponse;
import com.tarungoyaldev.android.noque.model.StoreInformation;
import com.tarungoyaldev.android.noque.model.StoreProduct;

import java.util.HashMap;
import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by tarungoyal on 11/07/16.
 */
public interface StoreService {
    @GET("noque/api/?")
    Observable<List<StoreInformation>> getStoreList(@Query("format") String format);

    @GET("noque/api/store_products/{storeId}/{barcode}/?")
    Observable<StoreProduct> getStoreProduct(@Path("storeId") long storeId,
                                             @Path("barcode") String barcode,
                                             @Query("format") String format);

    @POST("noque/api/login/")
    Observable<Customer> loginCustomer(@Body HashMap<String, String> body);

    @POST("/noque/orders/place_order/")
    Observable<PlaceOrderResponse> placeOrder(@Body StoreDetailsActivity.OrderBody body);
}
