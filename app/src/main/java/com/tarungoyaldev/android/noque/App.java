package com.tarungoyaldev.android.noque;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;

import com.tarungoyaldev.android.noque.model.Customer;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.ObjectGraph;

/**
 * Created by tarungoyal on 11/07/16.
 */
public class App extends Application {

    private ObjectGraph objectGraph;
    @Inject Config config;

    @Override public void onCreate() {
        super.onCreate();
        objectGraph = ObjectGraph.create(getModules().toArray());
        objectGraph.inject(this);
        System.out.println("In App.java, Config username: " + config.getBaseUrl());
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    private List<Object> getModules() {
        return Arrays.<Object>asList(new AppModule(this));
    }

    public ObjectGraph createScopedGraph(Object... modules) {
        return objectGraph.plus(modules);
    }

    public void setCustomer(Customer customer) {
        config.setCustomer(customer);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.auth_token), customer.getToken());
        editor.putString(getString(R.string.firstName), customer.getFirstName());
        editor.putString(getString(R.string.lastName), customer.getLastName());
        editor.putString(getString(R.string.email), customer.getEmail());
        editor.putLong(getString(R.string.userId), customer.getUserId());
        editor.putLong(getString(R.string.phone), customer.getPhone());
        editor.putString(getString(R.string.username), customer.getUsername());
        editor.apply();
    }

    public void initiateCustomer() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Customer customer = new Customer(
                sharedPref.getString(getString(R.string.auth_token), null),
                "" /* Message */,
                sharedPref.getString(getString(R.string.username), null),
                sharedPref.getString(getString(R.string.lastName), null),
                sharedPref.getLong(getString(R.string.userId), (long) 0),
                sharedPref.getString(getString(R.string.email), null),
                sharedPref.getString(getString(R.string.firstName), null),
                sharedPref.getLong(getString(R.string.phone), (long) 0)
        );
        config.setCustomer(customer);
    }
}
