package com.tarungoyaldev.android.noque;

import com.tarungoyaldev.android.noque.model.Customer;

/**
 * Created by tarungoyal on 11/07/16.
 */
public class Config {
    private String baseUrl = "http://noques.com/";
    private Customer customer = null;

    public Config() {
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }


    public String getBaseUrl() {
        return baseUrl;
    }
}
