package com.tarungoyaldev.android.noque;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import dagger.ObjectGraph;

/**
 * Created by tarungoyal on 13/07/16.
 */
public abstract class DaggerActivity extends AppCompatActivity {

    protected ObjectGraph mObjectGraph;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createObjectGraphIfNeeded();
        mObjectGraph.inject(this);
    }

    protected abstract Object getModule();

    protected void createObjectGraphIfNeeded() {
        if (mObjectGraph == null) {
            mObjectGraph = ((App) getApplication()).createScopedGraph(getModule());
        }
    }

    public void inject(Object object) {
        createObjectGraphIfNeeded();
        mObjectGraph.inject(object);
    }
}
